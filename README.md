# shadowblue main image &nbsp; [![pipeline status](https://gitlab.com/shadowblue/main/badges/main/pipeline.svg)](https://gitlab.com/shadowblue/main/-/commits/main) &nbsp; [![Quay Images](https://img.shields.io/badge/Quay-Images-green)](https://quay.io/shadowblue/main)

A desktop image built on top of [base image](https://gitlab.com/shadowblue/base) with some batteries for general desktop use.

Changes in this image in comparison with [base image](https://gitlab.com/shadowblue/base):

- Increased zram size. You can see formula in [zram-generator.conf](./config/files/usr/etc/systemd/zram-generator.conf)
- AMDGPU driver enabled for GCN 1 and 2 (Southern Islands and Sea Islands)
- GSP firmware support enabled in Nouveau driver
- Additional packages and gnome-shell extensions installed (see [recipes](./recipes))
- Changed some GNOME defaults (see [gschema overrides](./config/gschema-overrides/) and [dconf defaults](./config/files/usr/etc/dconf/db/local.d/))
- Added script to easily enable Betterfox user.js and Firefox GNOME theme:
  - Run `tweak_firefox` in your terminal to enable them
  - See `tweak_firefox -h` for non-interactive options
  - You can place your user.js overrides in `user-overrides.js` file inside your profile directory
  - Or you can place such overrides in `config/files/usr/share/shadowblue/userjs/overrides/` directory in your repo for image-wide overrides

P.S. You can see reasons for most of this changes in [recipes directory](./recipes).

> [!IMPORTANT]
> Only for those who want to modify this image. Others can scroll further.
>
> Please note that due to how default-flatpaks module works if you build derived image and make your own changes with default-flatpak module you will have to reapply all flatpak related changes from this image (default-flatpak module rewrites modifications from the upstream image). See [this issue](https://github.com/blue-build/modules/issues/231). Instead, you can change flatpak selection with [local modification](https://blue-build.org/reference/modules/default-flatpaks/#local-modification). Or do it by hands or other instruments like Ansible.

## Installation

> [!WARNING]
> [This is an experimental feature](https://www.fedoraproject.org/wiki/Changes/OstreeNativeContainerStable), try at your own discretion.

To rebase an existing atomic Fedora installation to the latest main image (see [available images](#available-images) and [available tags](#available-tags) for other options):

- **Recommended:** reset all modifications of immutable image (layered packages, overrides, etc):
  ```
  rpm-ostree reset
  ```
- Rebase to the unsigned image, to get the proper signing keys and policies installed:
  ```
  rpm-ostree rebase ostree-unverified-registry:quay.io/shadowblue/main:latest
  ```
- Reboot to complete the rebase:
  ```
  systemctl reboot
  ```
- Note that on the first boot after rebase, the system will remove all flatpak applications installed from the Fedora repo and install applications from Flathub. After installation, there is a chance that the icons of these applications will be missing. Don't panic, after the next rebase+reboot this should be fixed. If not, try switching the dark/light theme. If nothing helps, open the issue in this repo.
- Then rebase to the signed image, like so:
  ```
  rpm-ostree rebase ostree-image-signed:docker://quay.io/shadowblue/main:latest
  ```
- Reboot again to complete the installation
  ```
  systemctl reboot
  ```

## Schedule

These images start building every day at 04:30 UTC. Usually the images are ready at 04:40 UTC.

## Variants

### Supported versions

- Rawhide
- Branched on a best effort scenario. I can forget to enable it when branched starts in Fedora and enable it a few days or weeks later
- Latest Fedora release until official EoL
- Previous Fedora release until official EoL

### Available images

- `quay.io/shadowblue/main` — for all normal GPUs that don't require out-of-tree kernel modules (AMD, Intel)
- `quay.io/shadowblue/main-nvidia` — for Nvidia Maxwell and newer
- `quay.io/shadowblue/main-nvidia-470xx` — for Nvidia Kepler

### Available tags

- :rawhide (Not available for nvidia images)
- :latest (Tracks newest fedora version released. Version is replaced two weeks after the new release available.)
- :oldlatest (:latest version - 1)
- :${fedora_version} + 1 (e.g. :41. Available only if there is a branched version.)
- :${fedora_version} (e.g. :40)
- :${fedora_version} - 1 (e.g. :39)

Each tag has it's timestamp version for easier rollback and release pinning. E.g. :rawhide → :rawhide-20240523, :latest → :latest-20240523, etc.

### Tags expiration policy

All tags in registry for oldlatest version expire after 180 days. Tags for latest version expire after 365 days. Tags for rawhide and branched expire after 30 days.

## ISO

If build on Fedora Atomic, you can generate an offline ISO with the instructions available [here](https://blue-build.org/learn/universal-blue/#fresh-install-from-an-iso). These ISOs cannot unfortunately be distributed on GitLab for free due to large sizes, so for public projects something else has to be used for hosting.

## Verification

These images are signed with [Sigstore](https://www.sigstore.dev/)'s [cosign](https://github.com/sigstore/cosign). You can verify the signature by downloading the `cosign.pub` file from this repo and running the following command:

```bash
cosign verify --key cosign.pub quay.io/shadowblue/main:latest
```

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## License

All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.

```
Copyright 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
Copyright 2024 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

Portions of this software are copyright of their respective authors and were originally released under the MIT license:

- Files `user.js` and `user_hardened.js` inside `config/files/usr/share/shadowblue/` directory: Betterfox, Copyright © 2020 yokoffing. For licensing see LICENSE.Betterfox file
- File `config/files/usr/libexec/shadowblue/generate_userjs.sh`: Minimal safe Bash script template, Copyright © 2020 Maciej Radzikowski. For licensing see LICENSE.BashScriptTemplate file
