#!/usr/bin/env bash
# SPDX-FileCopyrightText: Copyright © 2024 Andrey Brusnik <dev@shdwchn.io>
#
# SPDX-License-Identifier: Apache-2.0

set -Eeuo pipefail

tag="126.0"

curl -L "https://github.com/yokoffing/Betterfox/archive/refs/tags/${tag}.tar.gz" -o betterfox.tar.gz
tar xf betterfox.tar.gz
mv "Betterfox-${tag}" /usr/share/betterfox
rm betterfox.tar.gz
